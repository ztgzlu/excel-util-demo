package com.example.demo;

import cn.hutool.core.collection.CollUtil;
import com.example.demo.model.ExcelModel;
import com.example.demo.model.User;
import com.example.demo.utils.ExcelUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@SpringBootTest
class ExcelUtilDemoApplicationTests {

    @Test
    void contextLoads() {
        ArrayList<ExcelModel> list = CollUtil.newArrayList();
        list.add(ExcelModel.of("unit", "单位").setMerged(true));
        list.add(ExcelModel.of("name", "姓名").setCenter(true));
        list.add(ExcelModel.of("addr", "地址"));
        list.add(ExcelModel.of("age", "年龄"));
        list.add(ExcelModel.of("createTime", "创建时间").setFormat("mm-dd"));
        list.add(ExcelModel.of("updateTime", "更新时间"));

        ArrayList<User> users = CollUtil.newArrayList();
        users.add(new User("六公司", "张三", "胡家庙", 34, LocalDateTime.now(), LocalDateTime.now()));
        users.add(new User("六公司", "李四", "张家堡", 14, LocalDateTime.now(), LocalDateTime.now()));
        users.add(new User("六公司", "王五", "小寨", 71, LocalDateTime.now(), LocalDateTime.now()));
        users.add(new User("六公司", "赵六", "高新", 49, LocalDateTime.now(), LocalDateTime.now()));

        try {
            FileOutputStream outputStream = new FileOutputStream("/Users/ztg/daochu/rwb.xls");
            ExcelUtil.listToExcel(users, list, "人员列表",2, outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
