package com.example.demo.model;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor(staticName = "of")
public class ExcelModel {
    /**
     * 表头
     */
    @NonNull
    private String enField;
    /**
     * 类名
     */
    @NonNull
    private String cnField;

    /**
     * 是否居中
     */
    private boolean center = false;
    /**
     * 是否合并居中
     */
    private boolean merged = false;

    /**
     * 日期格式，默认：yyyy-mm-dd
     */
    private String format = "yyyy-mm-dd";
}
