package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String unit;
    private String name;
    private String addr;
    private Integer age;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

}
